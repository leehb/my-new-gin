package redisDB

import (
	"fmt"
	"github.com/go-redis/redis"
	"my-new-gin/configs"
	"strconv"
	"sync"
)

var (
	RedisClient *redis.Client
	once sync.Once
)

func GetRedisClient(conf configs.RedisConf) (*redis.Client, error) {
	var err error
	db, _ := strconv.Atoi(conf.DB)
	once.Do(func() {
		RedisClient = redis.NewClient(&redis.Options{
			Addr: fmt.Sprintf("%s:%s",conf.Ip,conf.Port),
			Password:conf.Password,
			DB:db,
		})
	})
	_, err = RedisClient.Ping().Result()
	return RedisClient, err
}

func RedisConnClose() {
	RedisClient.Close()
}