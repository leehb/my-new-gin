package utils

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func GetFileInfo(filePath string) (fileInfo os.FileInfo, err error) {
	return os.Stat(filePath)
}

func ReadDirAllFiles(dirPath string) ([]string,error) {
	var fileArr []string
	if !strings.HasSuffix(dirPath,"/") {
		dirPath = dirPath + "/"
	}
	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return nil, err
	}
	for _, v := range files {
		if v.IsDir() {
			fs, err := ReadDirAllFiles(dirPath+v.Name())
			if err == nil {
				fileArr = append(fileArr,fs...)
			}
		} else {
			fileArr = append(fileArr,dirPath+v.Name())
		}
	}
	return fileArr, nil
}

func DeleteDir(dirPath string) error {
	if !strings.HasSuffix(dirPath,"/") {
		dirPath = dirPath + "/"
	}
	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return err
	}

	for _, v := range files {
		if v.IsDir() {
			err = DeleteDir(dirPath+v.Name())
			if err != nil {
				return err
			}
		}else{
			//删除文件
			err = os.Remove(dirPath+v.Name())
			fmt.Printf("remove file: %s\n",dirPath+v.Name())
			if err != nil {
				return err
			}
		}
	}
	err = os.Remove(dirPath)
	fmt.Printf("remove dir: %s\n",dirPath)
	if err != nil {
		return err
	}
	return nil
}

func DeleteFile(filePath string) error  {
	err := os.Remove(filePath)
	fmt.Printf("remove file: %s\n",filePath)
	if err != nil {
		return err
	}
	return nil
}

