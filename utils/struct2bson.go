package utils

import (
	"gopkg.in/mgo.v2/bson"
)

func Struct2Bson(obj interface{}) (bson.M, error) {

	js, err := Struct2Json(obj)
	if err != nil {
		return nil, err
	}
	mp, err := Json2Map(js)
	if err != nil {
		return nil, err
	}
	return bson.M(mp), nil
}
