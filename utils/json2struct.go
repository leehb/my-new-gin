package utils

import "encoding/json"

func Json2Struct(jsonstr string, st interface{}) (interface{}, error) {
	err := json.Unmarshal([]byte(jsonstr), st)
	if err != nil {
		return nil, err
	}
	return st, nil
}
