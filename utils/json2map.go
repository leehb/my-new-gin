package utils

import (
	"encoding/json"
	"fmt"
)

func Json2Map(jsonstr string) (map[string]interface{}, error) {
	m := make(map[string]interface{})
	err := json.Unmarshal([]byte(jsonstr), &m)
	if err != nil {
		return nil, fmt.Errorf("Json To Map ERROR: %v \n",err)
	}
	return m, nil
}
