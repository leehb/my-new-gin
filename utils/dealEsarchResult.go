package utils

import (
	"encoding/json"
	"github.com/olivere/elastic"
)

func DealEsarchResult(res *elastic.SearchResult) []*json.RawMessage {
	var arr []*json.RawMessage
	for _,v:= range res.Hits.Hits {
		arr = append(arr,v.Source)
	}
	return arr
}
