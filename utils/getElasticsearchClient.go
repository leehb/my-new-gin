package utils

import (
	"errors"
	"fmt"
	"github.com/olivere/elastic"
	"my-new-gin/configs"
	"my-new-gin/dbs/elasticsearch"
)

func GetElasticsearchClient() (*elastic.Client, error) {
	cli, err := elasticsearch.GetESClilent(configs.GetElasticsearchConf())
	if err != nil {
		fmt.Printf("server error -> %s",err.Error())
		return nil, errors.New("service error")
	}
	return cli, nil
}
