package utils

import (
	"crypto/md5"
	"encoding/base64"
)

func GetMD5(str string) string  {
	h := md5.New()
	h.Write([]byte(str)) // 需要加密的字符串
	return base64.StdEncoding.EncodeToString(h.Sum(nil))
}
