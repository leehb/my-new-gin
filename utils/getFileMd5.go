package utils

import (
	"crypto/md5"
	"encoding/base64"
	"fmt"
	"io/ioutil"
)

func GetFileMD5(filePath string) (string,error) {
	fileContent, err := ioutil.ReadFile(filePath)
	if err != nil {
		fmt.Printf("Read File ERROR : %v \n",err)
		return "",fmt.Errorf("Read File ERROR : %v \n",err)
	}
	h := md5.New()
	h.Write(fileContent)
	return base64.StdEncoding.EncodeToString(h.Sum(nil)), nil
}