package utils

import (
	"fmt"
	"github.com/goinggo/mapstructure"
)

func Map2Struct(m map[string]interface{},st interface{}) (interface{}, error)  {
	err := mapstructure.Decode(m, st)
	if err != nil {
		return nil, fmt.Errorf("Map To Struct ERROR: %v \n", err)
	}
	return st, nil
}