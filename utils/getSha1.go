package utils

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"io"
	"my-new-gin/configs"
	"strings"
)

func GetShe1ByKey(str string,key string) string {
	if strings.TrimSpace(key) == "" {
		key = configs.GetSha1KeyConf()
	}
	k := []byte(key)
	mac := hmac.New(sha1.New, k)
	mac.Write([]byte(str))
	return base64.StdEncoding.EncodeToString(mac.Sum(nil))
}

func GetShe1(str string) string {
	h := sha1.New()
	_, err := io.WriteString(h, str)
	if err != nil {
		return ""
	}
	return base64.StdEncoding.EncodeToString(h.Sum(nil))
}
