package utils

import "strings"

func Urls2String(urls []string) string {
	str := ""
	for _, url := range urls {
		str += url + "|"
	}
	return str[:len(str)-1]
}

func String2Urls(str string) []string {
	return strings.Split(str,"|")
}