package utils

import (
	"encoding/json"
	"github.com/pkg/errors"
	"io/ioutil"
	"net/http"
	"net/url"
)

type youdaoAPI struct {
	keyfrom string
	key     string
}

var youdaoAPIs = [...]youdaoAPI{
	{
		keyfrom: "xujiangtao", //1
		key:     "1490852988",
	},
	{
		keyfrom: "ltxywp", //2
		key:     "1092195550",
	},
	{
		keyfrom: "txw1958", //3
		key:     "876842050",
	},
	{
		keyfrom: "youdanfanyi123", //4
		key:     "1357452033",
	},
	{
		keyfrom: "fadabvaa", //5
		key:     "522071532",
	},
	{
		keyfrom: "yyxiaozhan", //6
		key:     "1696230822",
	},
	{
		keyfrom: "siwuxie095-test", //7
		key:     "2140200403",
	},
	{
		keyfrom: "11pegasus11", //8
		key:     "273646050",
	},
	{
		keyfrom: "webblog", //9
		key:     "1223831798",
	},
	{
		keyfrom: "wojiaozhh", //10
		key:     "1770085291",
	},
	{
		keyfrom: "atmoon", //11
		key:     "1407453772",
	},
	{
		keyfrom: "morninglight", //12
		key:     "1612199890",
	},
	{
		keyfrom: "Yanzhikai", //13
		key:     "2032414398",
	},
	{
		keyfrom: "JustForTestYouDao", //14
		key:     "498375134",
	},
	{
		keyfrom: "aaa123ddd", //15
		key:     "336378893",
	},
	{
		keyfrom: "aaa123ddd", //16
		key:     "336378893",
	},
	{
		keyfrom: "wjy-test", //17
		key:     "36384249",
	},
	{
		keyfrom: "youdao111", //18
		key:     "60638690",
	},
	{
		keyfrom: "pythonfankjjkj1", //19
		key:     "1288254626",
	},
	{
		keyfrom: "Dic-EVE", //20
		key:     "975360059",
	},
	{
		keyfrom: "youdianbao", //21
		key:     "1661829537",
	},
	{
		keyfrom: "AndroidHttpTest", //22
		key:     "507293865",
	},
	{
		keyfrom: "123licheng", //23
		key:     "1933182090",
	},
	{
		keyfrom: "pdblog", //24
		key:     "993123434",
	},
	{
		keyfrom: "testorot", //25
		key:     "1145972070",
	},
	{
		keyfrom: "node-translator", //1
		key:     "2058911035",
	},
	{
		keyfrom: "mytranslator1234", //2
		key:     "1501976072",
	},
	{
		keyfrom: "SkyHttpGetTest", //3
		key:     "545323935",
	},
	{
		keyfrom: "htttpGetTest", //4
		key:     "1230480132",
	},
	{
		keyfrom: "neverland", //5
		key:     "969918857",
	},
	{
		keyfrom: "HTTP-TESTdddaa", //6
		key:     "702271149",
	},
	{
		keyfrom: "fadabvaa", //7
		key:     "522071532",
	},
	{
		keyfrom: "atmoon", //8
		key:     "1407453772",
	},
	{
		keyfrom: "orchid", //9
		key:     "1008797533",
	},
	{
		keyfrom: "chdego", //10
		key:     "1347056326",
	},
	{
		keyfrom: "cxvsdffd33", //11
		key:     "1310976914",
	},
	{
		keyfrom: "123licheng", //12
		key:     "1933182090",
	},
	{
		keyfrom: "huichuang", //13
		key:     "386196262",
	},
	{
		keyfrom: "eweewqeqweqwe", //14
		key:     "957582233",
	},
	{
		keyfrom: "abc1243", //15
		key:     "1207861310",
	},
	{
		keyfrom: "xxxxxxx", //16
		key:     "1618693256",
	},
	{
		keyfrom: "mypydict", //17
		key:     "27855339",
	},
	{
		keyfrom: "zqhong", //18
		key:     "694644553",
	},
	{
		keyfrom: "wangtuizhijia", //19
		key:     "1048394636",
	},
	{
		keyfrom: "xinlei", //20
		key:     "759115437",
	},
	{
		keyfrom: "youdaoci", //21
		key:     "694691143",
	},
}
var ii int = -1
func Translate(keyword string) (string,error) {
	ii ++
	ii %= len(youdaoAPIs)
	ydapi := youdaoAPIs[ii]
	api := "http://fanyi.youdao.com/openapi.do?keyfrom="+ydapi.keyfrom+"&key="+ydapi.key+"&type=data&doctype=json&version=1.1&q="+url.QueryEscape(keyword)
	resp, err :=   http.Get(api)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	m := make(map[string]interface{})
	err = json.Unmarshal([]byte(body), &m)
	if err != nil {
		return "", err
	}
	data, ok := m["translation"]
	if !ok {
		return "", errors.New("Translate ERROR")
	}
	arr, ok := data.([]interface{})
	if !ok {
		return "", errors.New("Translate Data ERROR")
	}
	str, ok := arr[0].(string)
	if !ok {
		return "", errors.New("Translate String ERROR")
	}
	return str, nil
}