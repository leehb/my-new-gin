package schedule

import (
	"github.com/robfig/cron"
	"my-new-gin/schedule/jobs"
)

func init()  {
	c := cron.New()
	_ = c.AddFunc("@every 5m",jobs.Mysql2Elasticsearch)
	c.Start()
}