package entitys

import (
	"github.com/jinzhu/gorm"
	"log"
	"my-new-gin/configs"
	"my-new-gin/dbs/mysqlDB"
)

type EntityUser struct {
	gorm.Model
	Username string `json:"username" gorm:"type:varchar(20);not null;unique"`
	Password string `json:"password" gorm:"type:varchar(35);not null"`
	Name string `json:"name" gorm:"type:varchar(20)"`
	Sex int `json:"sex" gorm:"type:tinyint(1)"`
	Age int `json:"age" gorm:"type:tinyint(3)"`
}

func init()  {
	db, err := mysqlDB.GetDBConnection(configs.GetMysqlConf())
	if err != nil {
		log.Fatal(err.Error())
	}
	if !db.HasTable(&EntityUser{}) {
		db.Set("gorm:table_options", "ENGINE=InnoDB").CreateTable(&EntityUser{})
	}
}


