package commonfunc

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

type RespMessage struct {
	Code int
	Status string
	Token string
	Message interface{}
}

func ReqSuccess(c *gin.Context, message interface{})  {
	resp := RespMessage{
		Code:http.StatusOK,
		Status:http.StatusText(http.StatusOK),
		Message:message,
	}
	t, ok := c.Get("Token")
	if ok {
		resp.Token = t.(string)
	}
	c.JSON(http.StatusOK,resp)
}

func ReqUnauthorized(c *gin.Context, message interface{})  {
	c.JSON(http.StatusUnauthorized,RespMessage{
		Code:http.StatusUnauthorized,
		Status:http.StatusText(http.StatusUnauthorized),
		Message:message,
	})
}

func ReqFail(c *gin.Context, message interface{})  {
	c.JSON(http.StatusInternalServerError,RespMessage{
		Code:http.StatusInternalServerError,
		Status:http.StatusText(http.StatusInternalServerError),
		Message:message,
	})
}

func ReqNotFound(c *gin.Context, message interface{})  {
	c.JSON(http.StatusNotFound,RespMessage{
		Code:http.StatusNotFound,
		Status:http.StatusText(http.StatusNotFound),
		Message:message,
	})
}

func ReqMissParams(c *gin.Context, message interface{})  {
	c.JSON(http.StatusBadRequest,RespMessage{
		Code:http.StatusBadRequest,
		Status:http.StatusText(http.StatusBadRequest),
		Message:fmt.Sprintf("Miss Params: %#v\n",message),
	})
}


