FROM alpine:latest

RUN mkdir /etc/supervisor.d

RUN mkdir /app

RUN mkdir /app/app_logs

#RUN echo -e "https://mirrors.aliyun.com/alpine/v3.7/main" > /etc/apk/repositories

RUN apk update && apk add openssh-server && apk add openssh-client && apk add supervisor && apk add vim

RUN mkdir -p /var/run/sshd

ADD bin/app /app/app

ADD configs/app_config.json /app/app_config.json

ADD bin/app_supervisor.ini /etc/supervisor.d/app_supervisor.ini

EXPOSE 22 8080

WORKDIR /app

CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]

