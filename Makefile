build:
	GOOS=linux GOARCH=amd64 go build -o ./bin/app ./main.go
	docker build -t my-new-gin ./

run:
	docker run -d -p 22:22 -p 8080:8080 --name my-new-gin my-new-gin

rebuild:
	GOOS=linux GOARCH=amd64 go build -o ./bin/app ./main.go
	docker cp ./bin/app my-new-gin:/app/app
	docker cp ./configs/app_config.json my-new-gin:/app/app_config.json
	docker restart my-new-gin

startmysql:
	docker start mysql

startredis:
	docker start redis