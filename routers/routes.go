package routers

import (
	"github.com/gin-gonic/gin"
	"my-new-gin/controllers"
	"my-new-gin/middleware"
	ws "my-new-gin/socket"
	"net/http"
)
//注册普通路由
func routeManager(route *gin.Engine) {
	route.GET("/test", func(ctx *gin.Context) {
		ctx.JSON(200,"test success")
	})
	handlerUser := new(controllers.HandlerUser)
	userRoutes := route.Group("/user")
	{
		userRoutes.POST("/login",handlerUser.Login)
		userRoutes.POST("/register",handlerUser.Register)
		userRoutes.POST("/getuserlist",middleware.Authorization(),handlerUser.GetUserList)
	}
}

func RegisterRoutes(route *gin.Engine) {
	//普通路由
	routeManager(route)
	//静态资源
	route.Static("/assets", "../assets")
	route.StaticFS("/more_static", http.Dir("../assets"))
	route.StaticFile("/favicon.ico", "../assets/a.png")
	//websocket
	route.GET("/ws/:uuid", ws.Ws)
}





